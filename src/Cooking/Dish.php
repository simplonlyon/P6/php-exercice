<?php

namespace App\Cooking;

use App\Cooking\Ingredient;

class Dish extends Ingredient {
    private $ingredients;

    public function __construct() {
        parent::__construct("", 0, true);
        $this->ingredients = [];
    }

    public function addIngredient(Ingredient $paramIngredient): void {
        //On ajoute le nouvel ingrédient à la liste des ingrédient du plat
        $this->ingredients[] = $paramIngredient;
        //On ajoute aux calories du plat les calories du nouvel ingrédient
        $this->calories += $paramIngredient->calories;
        //On vérifie si le plat est actuellement végétal et si l'ingrédient qu'on rajoute n'est pas végétal
        if($this->vegetal && !$paramIngredient->vegetal) {
            $this->vegetal = false;
        }
    }

    public function cook():void {
        if($this->raw) {
            //On remet les calories à zéro pour les recalculer post cuisson
            $this->calories = 0;
            //On itère sur les ingrédients du plat
            foreach($this->ingredients as $itemIngr) {
                //On cuit chaque ingrédient du plat
                $itemIngr->cook();
                //On ajoute les calories de l'ingrédient cuit à celle du plat
                $this->calories += $itemIngr->calories;
            } 
            //On passe le plat à pas cru
            $this->raw = false;        
        }
    }
}