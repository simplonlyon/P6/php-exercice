<?php

namespace App\Cooking;

class Ingredient {
    public $name;
    public $calories;
    public $vegetal;
    public $raw;

    public function __construct(string $paramName, int $paramCalories, bool $paramVegetal, bool $paramRaw = true) {
        $this->name = $paramName;
        $this->calories = $paramCalories;
        $this->vegetal = $paramVegetal;
        $this->raw = $paramRaw;
    }

    public function cook(): void {
        if($this->raw) {
            $this->raw = false;
            $this->calories += 10;
        }
    }
}