<?php

namespace App\Entities;

class User{
  public $username;
  public $email;
  public $birthdate;
  private $password;

  public function __construct(string $paramUsername, string $paramEmail, \DateTime $paramBirthdate, string $paramPassword)
  {
    $this->username = $paramUsername;
    $this->email = $paramEmail;
    $this->birthdate = $paramBirthdate;
    $this->password = $paramPassword;
  }  
}